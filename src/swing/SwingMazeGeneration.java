package swing;

import common.MazeModel;
import common.PrimeMazeGenerator;

/**
 * Maze generation demonstration using Swing
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class SwingMazeGeneration{

	private static final int ROWS = 41, COLUMNS = 29;
	private static final long DELAY = 10;

	public SwingMazeGeneration() {
		MazeModel model = new MazeModel(ROWS, COLUMNS);
		View view = new View(model);
		view.addActionListener(e->{
			 new PrimeMazeGenerator(model.getCellModels()).setDelay(DELAY).execute();
		});
		view.showView();
	}

	public static void main(String[] args) {

		new SwingMazeGeneration();
	}
}

