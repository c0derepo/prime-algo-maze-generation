/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

/**
 * Maze representation
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class MazeModel {

	/**
	 * Collection to represent an entire maze
	 */
	private final CellModel[][] cellModels;

	public MazeModel(int rows, int columns) {

		cellModels = new CellModel[rows][columns];
		for(int row=0; row <cellModels.length; row++) {
			for(int col=0; col<cellModels[row].length; col++) {
				CellModel cellModel = new CellModel(row, col);
				cellModels[row][col] = cellModel;
			}
		}
	}

	/**
	* Get {@link #cellModels}
	*/
	public CellModel[][] getCellModels() {
		return cellModels;
	}
}
