package javafx;

import common.MazeModel;
import common.PrimeMazeGenerator;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Maze generation demonstration using JavaFx
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class JavaFxMazeGeneration extends Application{

	private static final int ROWS = 41, COLUMNS = 29;
	private static final long DELAY = 10;

	@Override
	public void start(Stage primaryStage) {

		MazeModel model = new MazeModel(ROWS, COLUMNS);
		View view = new View(model);
		view.addActionListener(e->{
			 new PrimeMazeGenerator(model.getCellModels()).setDelay(DELAY).execute();
		});
		view.showView(primaryStage);
	}

	public static void main(String[] args) {
		launch(null);
	}
}