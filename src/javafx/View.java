package javafx;

import java.beans.PropertyChangeSupport;

import common.CellModel;
import common.MazeModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
class View {

	private static final int GAP = 2;
	private final Pane panel;
	private final Button newMaze;

	View(MazeModel model) {

		CellModel[][] cells = model.getCellModels();
		GridPane grid = new GridPane();
		grid.setVgap(GAP); 	grid.setHgap(GAP);
		grid.setStyle("-fx-border-color: black ; -fx-border-width:"+ GAP);

		CellView[][] cellViews = new CellView[cells.length][cells[0].length];
		for(int row=0; row <cellViews.length; row++) {
			for(int col=0; col<cellViews[row].length; col++) {
				CellView cellView = new CellView(cells[row][col]);
				cellViews[row][col] = cellView;
				grid.getChildren().add(cellView);
				GridPane.setConstraints(cellView, col, row);
			}
		}

		newMaze = new Button("Generate Maze");
		panel = new BorderPane(grid, null, null, newMaze, null);
	}

	void addActionListener(EventHandler<ActionEvent> handler){
		newMaze.setOnAction(handler);
	}

	void showView(Stage stage){

		stage.setTitle("Prime's Algorithm Demo");
		Scene scene = new Scene(panel);
		stage.setScene(scene);
		stage.show();
	}

	/**
	* Get {@link #panel}
	*/
	public Pane getPanel() {
		return panel;
	}
}

//Single cell view
class CellView extends Label {

	private static int CELL_H =15, CELL_W = 15;
	private final CellModel cellModel;
	private final PropertyChangeSupport pcs;

	CellView(CellModel cellModel) {
		this.cellModel = cellModel;
		pcs = new PropertyChangeSupport(this);
		pcs.addPropertyChangeListener(evt -> {
			//change background color
			Platform.runLater(()-> setStyle("-fx-background-color:"+  (isWall() ?  "black" : "white")  +";"));
		});
		cellModel.setPropertChangeSupport(pcs);
		setPrefSize(CELL_W, CELL_H);
	}

	int getRow() {
		return cellModel.getRow();
	}

	int getColumn() {
		return cellModel.getColumn();
	}

	void setWall(boolean isWall) {
		cellModel.setWall(isWall);
	}

	boolean isWall() {
		return cellModel.isWall();
	}

	@Override
	public String toString() {
		return cellModel.toString() ;
	}
}
